#ifndef SPRITEGROUP_H
#define SPRITEGROUP_H
#include <memory>
#include <list>
#include "../External/glm/glm.hpp"

class Sprite;
class RenderGroup;
class SpriteGroup {
    public:
        virtual std::shared_ptr<Sprite>& attach(std::shared_ptr<Sprite> sprite);
        virtual void detach(const std::shared_ptr<Sprite>& sprite);
        glm::vec2 getPosition(void) {
            return position;
        };
        virtual void setPosition(glm::vec2 xy) {
            position = xy;
        };
        virtual glm::vec2 getSize(void) {
            return size;
        };
        virtual void setSize(glm::vec2 wh) {
            size = wh;
        };
        virtual glm::vec2 getScale(void) {
            return glm::vec2(1.0f);
        };
        virtual float getRotation(void) {
            return 0.0f;
        };
        bool isVisible(void) {
            return visible;
        };
        int getZIndex(void) {
            return zIndex;
        };
    protected:
        virtual RenderGroup* addSubtree(RenderGroup* parent);
        virtual void removeSubtree(RenderGroup* parent);
        std::list<RenderGroup*> instances;
        std::list<std::shared_ptr<Sprite>> attached;
        bool contains(const SpriteGroup* sprite) const;
        SpriteGroup(void): position(0.0f), size(0.0f), zIndex(0), visible(true) {};
        int zIndex;
        glm::vec2 position;
        glm::vec2 size;
        bool visible;
};

#endif /* end of include guard: SPRITEGROUP_H */
