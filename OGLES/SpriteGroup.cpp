#include "SpriteGroup.h"
#include "Sprite.h"
#include "RenderGroup.h"
#include <algorithm>
#include <iostream>

std::shared_ptr<Sprite>& SpriteGroup::attach(std::shared_ptr<Sprite> sprite)
{
    if (sprite->contains(this))
        throw std::runtime_error("Adding this sprite produces cycle");
    attached.push_front(sprite);
    for (auto instance : instances)
        sprite->addSubtree(instance);
    return attached.front();
}

void SpriteGroup::detach(const std::shared_ptr<Sprite>& sprite)
{
    auto i = std::find(begin(attached), end(attached), sprite);
    if (i != end(attached)) {
        for (auto instance : instances)
            sprite->removeSubtree(instance);
        attached.erase(i);
    }
}

bool SpriteGroup::contains(const SpriteGroup* sprite) const
{
    if (this == sprite) return true;
    for (const auto& child : attached) {
        if (child->contains(sprite)) return true;
    }
    return false;
}

RenderGroup* SpriteGroup::addSubtree(RenderGroup* parent)
{
    instances.push_front(parent->addRenderGroup(this));
    for (auto sprite : attached)
        sprite->addSubtree(instances.front());
    return instances.front();
}

void SpriteGroup::removeSubtree(RenderGroup* parent)
{
    auto instance = std::find_if(begin(instances), end(instances), [parent](const RenderGroup * i) {
        return i->parent == parent;
    });
    if (instance != end(instances)) {
        for (auto sprite : attached)
            sprite->removeSubtree(*instance);
        parent->removeSubtree(*instance);
        instances.erase(instance);
    }
}