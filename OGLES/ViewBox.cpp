#include "ViewBox.h"
#include "SpriteGroup.h"
#define GLM_FORCE_RADIANS
#include "../External/glm/gtc/matrix_transform.hpp"

void ViewBox::validateTransform(void)
{
    transform = glm::ortho(owner->getPosition().x, owner->getSize().x, owner->getPosition().y, owner->getSize().y);
    isTransformDirty = false;
    for (auto& item : items)
        item->validateTransform();
}