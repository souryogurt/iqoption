#ifndef SPRITE_H
#define SPRITE_H
#include "SpriteGroup.h"
#include <memory>
#include <forward_list>
#include "../External/glm/glm.hpp"

class RenderGroup;
class Sprite: public SpriteGroup {
    public:
        Sprite(void): SpriteGroup(), scale(1.0f, 1.0f), rotation(0.0f) {};
        void setPosition(glm::vec2 xy);
        void setScale(glm::vec2 wh);
        void setRotation(float angleDegrees);
        virtual glm::vec2 getScale(void) {
            return scale;
        };
        virtual float getRotation(void) {
            return rotation;
        };
        virtual glm::vec2 getSize(void) {
            //TODO: should return self bounding box of all attached childs?
            return size;
        };
        virtual void setSize(glm::vec2 wh) {
            SpriteGroup::setSize(wh);
            //TODO: recalculate scale to make a size of bounding box == wh?
            //TODO: mark all instances dirty
            //for (auto instance : instances)
            //    instances->transformDirty();
        };
        void setVisible(bool flag);
        void setZIndex(int index);
    protected:
        glm::vec2 scale;
        float rotation;
};

#endif /* end of include guard: SPRITE_H */
