#include "Shape.h"
#include "RenderGroup.h"

Shape::Shape(RenderGroup* p): RenderInstance(p)
{
}

void Shape::validateTransform(void)
{
    transform = parent->transform;
}

void Shape::validateVisible(void)
{
    visible = parent->visible;
}

void Shape::validateZ(void)
{
    order = 0;
    zIndex = parent->zIndex;
}

void Shape::sortAndAssignZ(int level, int& n)
{
    level = level;
    order = n++;
}

RenderGroup* Shape::getRoot(void)
{
    auto root = parent;
    while (root->parent != nullptr)
        root = root->parent;
    return root;
}

Shape::~Shape(void)
{
    getRoot()->removeShape(this);
};
