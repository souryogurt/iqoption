#ifndef APPVIEW_H
#define APPVIEW_H
#include <memory>
#include "PVRApi/PVRApi.h"
#include "PVRUIRenderer/PVRUIRenderer.h"

class IQOptionTest;

class AppView {
    public:
        AppView(IQOptionTest& parent);
        void update(pvr::float32 dt);
        virtual ~AppView(void);

        void setBackground(const std::string& filename);
        pvr::api::SecondaryCommandBuffer& getCommandBuffer(void) {
            return cmdBuffer;
        };
    protected:
        bool dirty;
        glm::vec4 geometry;
        IQOptionTest& app;
        pvr::api::AssetStore assetStore;
        pvr::ui::UIRenderer uiRenderer;
        pvr::api::SecondaryCommandBuffer cmdBuffer;
        pvr::ui::MatrixGroup widgetGroup;
        pvr::ui::Image background;
        pvr::ui::Text someText;
        void updateCommandBuffer(void);
};

#endif /* end of include guard: APPVIEW_H */
