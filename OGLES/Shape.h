#ifndef SHAPE_H
#define SHAPE_H
#include "RenderInstance.h"

class Shape: public RenderInstance {
    public:
        Shape(RenderGroup* p);
        int order;
        int level;
    protected:
        virtual void validateTransform(void);
        virtual void validateVisible(void);
        virtual void validateZ(void);
        virtual void sortAndAssignZ(int level, int& order);
        virtual RenderGroup* getRoot(void);
        virtual ~Shape(void);
};

#endif /* end of include guard: SHAPE_H */
