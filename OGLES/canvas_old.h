#ifndef CANVAS_H
#define CANVAS_H

class Sprite;
class SpriteGroup {
    public:
        virtual std::shared_ptr<Sprite>& attach(std::shared_ptr<Sprite> sprite) {
            if (sprite->contains(this))
                throw std::runtime_error("Adding this sprite produces cycle");
            attached.push_front(sprite);
            return sprite;
        };
        virtual detach(Sprite* sprite) {
            auto i = std::find(begin(attached), end(attached), std::shared_ptr<Sprite>(sprite));
            if (i != end(attached))
                attached.erase(i);
        };
        glm::vec2 getPosition(void) {
            return position;
        };
        virtual void setPosition(glm::vec2 xy) {
            position = xy;
        };
        virtual glm::vec2 getSize(void) {
            return size;
        };
        virtual setSize(glm::vec2 wh) {
            size = wh;
        };
        virtual glm::vec2 getScale(void) {
            return glm::vec2(1.0f);
        };
        virtual float getRotation(void) {
            return 0.0f;
        };
    protected:
        std::list<std::shared_ptr<Sprite>> attached;
        bool contains(const Sprite* sprite) const {
            if (this == sprite) return true;
            for (const auto& child : attached) {
                if (child->contains(sprite)) return true;
            }
            return false;
        };
        SpriteGroup(void): position(0.0f), size(0.0f), zIndex(0) {};
        virtual ~SpriteGroup(void) {};
        int zIndex;
        glm::vec2 position;
        glm::vec2 size;
};

class Sprite: public SpriteGroup {
    public:
        Sprite(void): SpriteGroup(), scale(1.0f, 1.0f), rotation(0.0f) {};
        virtual std::shared_ptr<Sprite>& attach(std::shared_ptr<Sprite> sprite) {
            SpriteGroup::attach(sprite);
            for (auto instance : instances)
                sprite->addSubtree(instance);
            return sprite;
        };
        virtual void detach(Sprite* sprite) {
            SpriteGroup::detach(sprite);
            for (auto instance : instances)
                sprite->removeSubtree(instance);
        };
        void setPosition(glm::vec2 xy) {
            SpriteGroup::setPosition(xy);
            for (auto instance : instances)
                instance->transformDirty();
        };
        void setScale(glm::vec2 wh) {
            scale = wh;
            //TODO: recalculate size. size = originalSize*scale;
            for (auto instance : instances)
                instance->transformDirty();
        };
        void setRotation(float angleDegrees) {
            rotation = angleDegrees;
            for (auto instance : instances)
                instances->transformDirty();
        };
        virtual glm::vec2 getScale(void) {
            return scale;
        };
        virtual float getRotation(void) {
            return rotation;
        };
        virtual glm::vec2 getSize(void) {
            //TODO: should return self bounding box of all attached childs?
            return size;
        };
        virtual setSize(glm::vec2 wh) {
            SpriteGroup::setSize(wh);
            //TODO: recalculate scale to make a size of bounding box == wh?
            //TODO: mark all instances dirty
            //for (auto instance : instances)
            //    instances->transformDirty();
        };
    protected:
        virtual void addSubtree(RenderGroup* parent) {
            instances.push_front(parent->addRenderGroup());
        };
        virtual void removeSubtree(RenderGroup* parent) {
            if (!instances.empty()) {
                for (auto sprite : attached)
                    sprite->removeSubtree(instances.front());
                parent->removeSubtree(instances.front());
                instances.pop_front();
            }
        };
        std::forward_list<RenderGroup*> instances;
        glm::vec2 scale;
        float rotation;
};

class Canvas: public SpriteGroup {
    public:
        Canvas(glm::vec2 origin, glm::vec2 wh): SpriteGroup(), position(origin), size(wh), view(this) {};
        virtual std::shared_ptr<Sprite>& attach(std::shared_ptr<Sprite> sprite) {
            SpriteGroup::attach(sprite);
            sprite->addSubtree(view);
            return sprite;
        };
        virtual void detach(Sprite* sprite) {
            SpriteGroup::detach(sprite);
            sprite->removeSubtree(view);
        };
        virtual void setPosition(glm::vec2 xy) {
            SpriteGroup::setPosition(xy);
            view->transformDirty();
        };
        virtual setSize(glm::vec2 wh) {
            size = wh;
            view->transformDirty();
        };
    protected:
        ViewBox view;
};

#endif /* end of include guard: CANVAS_H */
