#define GLM_SWIZZLE
#include "AppView.h"
#include "IQOptionTest.h"

AppView::AppView(IQOptionTest& parent): app(parent), dirty(true)
{
    assetStore.init(app);
    uiRenderer.init(app.getGraphicsContext(), app.getFBO()->getRenderPass(), 0);
    cmdBuffer = app.getGraphicsContext()->createSecondaryCommandBufferOnDefaultPool();
    geometry = glm::vec4(glm::vec2(0.0f, 0.0f), uiRenderer.getRenderingDim());
    widgetGroup = uiRenderer.createMatrixGroup();
    someText = uiRenderer.createText("FUCK YOU!");
    widgetGroup->setViewProjection(uiRenderer.getScreenRotation()*uiRenderer.getProjection());
    widgetGroup->setScaleRotateTranslate(glm::translate(glm::vec3(geometry.xy() + glm::vec2(100, 100), 0.0f)));
    widgetGroup->commitUpdates();
}

void AppView::setBackground(const std::string& filename)
{
    pvr::api::TextureView texture;
    pvr::assets::TextureHeader header;
    if (!assetStore.getTextureWithCaching(app.getGraphicsContext(), filename, &texture, &header))
        throw std::runtime_error("Failed to load texture");
    widgetGroup->remove(background);
    background = uiRenderer.createImage(texture, header.getWidth(), header.getHeight());
    glm::vec2 textureSize((float)background->getWidth(), (float)background->getHeight());
    glm::vec2 widgetSize = geometry.zw() - geometry.xy();
    background->setScale(widgetSize / textureSize);
    background->setAnchor(pvr::ui::Anchor::Center, -1.0f, 1.0f);
    widgetGroup->add(background);
    widgetGroup->add(someText);
    widgetGroup->commitUpdates();
    dirty = true;
}

void AppView::updateCommandBuffer(void)
{
    if (dirty) {
        cmdBuffer->beginRecording(app.getFBO()->getRenderPass());
        uiRenderer.beginRendering(cmdBuffer);
        {
            widgetGroup->render();
            //background->render();
            //someText->render();
            uiRenderer.getSdkLogo()->render();
        }
        uiRenderer.endRendering();
        cmdBuffer->endRecording();
        //rerecord parent's buffer
        app.updateCommandBuffer();
        dirty = false;
    }
}

void AppView::update(pvr::float32 dt)
{
    //TODO: animate UI
    //TODO: update all childs

    updateCommandBuffer();
}

AppView::~AppView(void)
{
    uiRenderer.release();
    assetStore.releaseAll();
    background.reset();
    cmdBuffer.reset();
}
