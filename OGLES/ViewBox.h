#ifndef VIEWBOX_H
#define VIEWBOX_H
#include "RenderGroup.h"

class ViewBox: public RenderGroup {
    public:
        virtual void validateTransform(void);
        ViewBox(SpriteGroup* spriteGroup, RenderGroup* p): RenderGroup(spriteGroup, p) {};
};

#endif /* end of include guard: VIEWBOX_H */
