#include "RenderGroup.h"
#include "SpriteGroup.h"
#include "Shape.h"
#include "ViewBox.h"
#define GLM_FORCE_RADIANS
#include "../External/glm/gtc/matrix_transform.hpp"
#include <algorithm>

RenderGroup* RenderGroup::addRenderGroup(SpriteGroup* sprite)
{
    auto instance = new RenderGroup(sprite, this);
    //Insert sorted based on zIndex? Or sort later?
    //look std::upper_bound()
    items.push_back(std::unique_ptr<RenderInstance>(instance));
    instance->markTransformDirty();
    instance->markVisibleDirty();
    instance->markZDirty();
    return instance;
}

ViewBox* RenderGroup::addViewBox(SpriteGroup* sprite)
{
    auto instance = new ViewBox(sprite, this);
    //Insert sorted based on zIndex? Or sort later?
    //look std::upper_bound()
    items.push_back(std::unique_ptr<RenderInstance>(instance));
    instance->markTransformDirty();
    instance->markVisibleDirty();
    instance->markZDirty();
    return instance;
}

RenderGroup* RenderGroup::getRoot(void)
{
    auto root = this;
    while (root->parent != nullptr)
        root = root->parent;
    return root;
}

void RenderGroup::addShape(void)
{
    auto instance = new Shape(this);
    //Insert sorted based on zIndex? Or sort later?
    //look std::upper_bound()
    items.push_back(std::unique_ptr<RenderInstance>(instance));
    getRoot()->addShape(instance);
}

void RenderGroup::removeSubtree(RenderInstance* tree)
{
    items.remove_if([tree](const std::unique_ptr<RenderInstance>& i) {
        return i.get() == tree;
    });
}

void RenderGroup::markTransformDirty(void)
{
    if (!isTransformDirty) {
        isTransformDirty = true;
        getRoot()->addDirtyTransform(this);
    }
}

void RenderGroup::markVisibleDirty(void)
{
    if (!isVisibleDirty) {
        isVisibleDirty = true;
        getRoot()->addDirtyVisible(this);
    }
}

void RenderGroup::markZDirty(void)
{
    if (!isZDirty) {
        isZDirty = true;
        getRoot()->addDirtyZIndex(this);
    }
}

void RenderGroup::validateTransform(void)
{
    transform = glm::translate(parent->transform, glm::vec3(owner->getPosition(), 0.0f));
    transform = glm::rotate(transform, glm::radians(owner->getRotation()), glm::vec3(0.0f, 0.0f, 1.0f));
    transform = glm::scale(transform, glm::vec3(owner->getScale(), 1.0f));
    isTransformDirty = false;
    for (auto& item : items)
        item->validateTransform();
}

void RenderGroup::validateVisible(void)
{
    visible = parent->visible && owner->isVisible();
    isVisibleDirty = false;
    for (auto& item : items)
        item->validateVisible();
}

void RenderGroup::validateZ(void)
{
    zIndex = owner->getZIndex();
    isZDirty = false;
    for (auto& item : items)
        item->validateZ();
}

void RenderGroup::sortAndAssignZ(int level, int& n)
{
    //its stable already for the std::list, so elements with the same zIndex will
    //not swapped
    items.sort([](const std::unique_ptr<RenderInstance>& lhs, const std::unique_ptr<RenderInstance>& rhs) {
        return lhs->zIndex < rhs->zIndex;
    });
    for (auto& item : items)
        item->sortAndAssignZ(level + 1, n);
}

RenderGroup::RenderGroup(SpriteGroup* spriteGroup, RenderGroup* p) :
    RenderInstance(p), owner(spriteGroup), isTransformDirty(false), isVisibleDirty(false), isZDirty(false)
{
}

RenderGroup::~RenderGroup(void)
{
    getRoot()->removeRenderGroup(this);
}

void RenderGroup::addShape(Shape*)
{
    throw std::runtime_error("trying to add shape not to painter");
}

void RenderGroup::removeShape(Shape*)
{
}

void RenderGroup::addDirtyTransform(RenderGroup*)
{
    throw std::runtime_error("trying to add dirty transform not to painter");
}

void RenderGroup::addDirtyVisible(RenderGroup*)
{
    throw std::runtime_error("trying to add dirty visible not to painter");
}

void RenderGroup::addDirtyZIndex(RenderGroup*)
{
    throw std::runtime_error("trying to add dirty z not to painter");
}

void RenderGroup::removeRenderGroup(RenderGroup*)
{
}
