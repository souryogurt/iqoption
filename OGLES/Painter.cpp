#include "Painter.h"
#include "Shape.h"
#include <algorithm>

Painter::Painter(SpriteGroup* canvas):
    RenderGroup(canvas, nullptr), shapesChanged(false)
{
}

void Painter::addShape(Shape* shape)
{
    displayList.push_back(shape);
    shapesChanged = true;
}

void Painter::removeShape(Shape* shape)
{
    displayList.remove(shape);
    shapesChanged = true;
}

void Painter::addDirtyTransform(RenderGroup* group)
{
    dirtyTransforms.push_front(group);
}

void Painter::addDirtyVisible(RenderGroup* group)
{
    dirtyVisible.push_front(group);
}

void Painter::addDirtyZIndex(RenderGroup* group)
{
    dirtyZIndex.push_front(group);
}

void Painter::removeRenderGroup(RenderGroup* group)
{
    dirtyTransforms.remove(group);
    dirtyVisible.remove(group);
    dirtyZIndex.remove(group);
}

void Painter::validateDirtyTransforms(void)
{
    for (auto group : dirtyTransforms) {
        if (group->isTransformDirty) {
            auto topmostDirty = group;
            auto current = group;
            while (current->parent) {
                current = current->parent;
                if (current->isTransformDirty)
                    topmostDirty = current;
            }
            topmostDirty->validateTransform();
        }
    }
    dirtyTransforms.clear();
}

bool Painter::validateDirtyVisible(void)
{
    bool rebuiltCommandQueue = false;
    if (!dirtyVisible.empty()) {
        for (auto group : dirtyVisible) {
            if (group->isVisibleDirty) {
                auto topmostDirty = group;
                auto current = group;
                while (current->parent) {
                    current = current->parent;
                    if (current->isVisibleDirty)
                        topmostDirty = current;
                }
                topmostDirty->validateVisible();
            }
        }
        dirtyVisible.clear();
        rebuiltCommandQueue = true;
    }
    return rebuiltCommandQueue;
}

bool Painter::validateDirtyZIndex(void)
{
    bool rebuiltCommandQueue = false;
    if (!dirtyZIndex.empty()) {
        for (auto group : dirtyZIndex) {
            if (group->isZDirty) {
                auto topmostDirty = group;
                auto current = group;
                while (current->parent) {
                    current = current->parent;
                    if (current->isZDirty)
                        topmostDirty = current;
                }
                topmostDirty->validateZ();
            }
        }
        dirtyZIndex.clear();
        //Go whole tree, sort elements by z-index and assign z to shapes
        int initialZ = 0;
        sortAndAssignZ(0, initialZ);
        //sort by level, then by order
        displayList.sort([](Shape * lhs, Shape * rhs) {
            if (lhs->level != rhs->level)
                return (lhs->level < rhs->level);
            return (lhs->order < rhs->order);
        });
        rebuiltCommandQueue = true;
    }
    return rebuiltCommandQueue;
}

void Painter::render(pvr::api::Fbo& fbo)
{
    bool rebuildCommandQueue = shapesChanged;
    validateDirtyTransforms();
    rebuildCommandQueue |= validateDirtyVisible();
    rebuildCommandQueue |= validateDirtyZIndex();
    if (rebuildCommandQueue) {
        //Shapes already in paint order. First goes shapes on
        //a lower level, then shapes in next level/layer
        //Right UI renderer will group shapes to layers and
        //then render each layer independently. And finaly will
        //composite all layers to fbo.
        //But this reallly a lot of work. Common, this is test app.
        for (auto shape : displayList) {
            if (shape->visible) {
                //TODO: render this shape
                //TODO: set pipeline if currentPipeline!=shape->pipeline
                //TODO: set vbo if currentVbo!= shape->vbo
                //TODO: set texture if currentTextures!= shape->textures
                //TODO: set uniforms for shape
                //TODO: draw shape
            }
        }
        shapesChanged = false;
    }
    //TODO: submit queue
};
