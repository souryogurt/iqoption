#ifndef CANVAS_H
#define CANVAS_H
#include "../External/glm/glm.hpp"
#include "SpriteGroup.h"
#include "Painter.h"
#include "PVRApi/PVRApi.h"

class Canvas: public SpriteGroup {
    public:
        Canvas(glm::vec2 origin, glm::vec2 wh);
        virtual void setPosition(glm::vec2 xy);
        virtual void setSize(glm::vec2 wh);
        void render(pvr::api::Fbo& fbo);
    protected:
        Painter renderer;
};

#endif /* end of include guard: CANVAS_H */
