#ifndef IQOPTIONTEST_H
#define IQOPTIONTEST_H

#include "PVRShell/PVRShell.h"
#include "PVRApi/PVRApi.h"
#include "Canvas.h"

class IQOptionTest : public pvr::Shell {
    public:
        virtual pvr::Result::Enum initApplication();
        virtual pvr::Result::Enum initView();
        virtual pvr::Result::Enum releaseView();
        virtual pvr::Result::Enum quitApplication();
        virtual pvr::Result::Enum renderFrame();
        virtual void eventMappedInput(pvr::SimplifiedInput::Enum key);
    protected:
        std::shared_ptr<Canvas> canvas;
        pvr::api::Fbo screen;
        pvr::uint64 lastTime = 0;

};
#endif /* end of include guard: IQOPTIONTEST_H */
