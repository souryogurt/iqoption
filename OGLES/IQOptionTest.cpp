#include "IQOptionTest.h"
#include "Image.h"
#include <memory>

pvr::Result::Enum IQOptionTest::initApplication()
{
    return pvr::Result::Success;
}

pvr::Result::Enum IQOptionTest::quitApplication()
{
    return pvr::Result::Success;
}

pvr::Result::Enum IQOptionTest::initView()
{
    screen = getGraphicsContext()->createOnScreenFbo(0);
    canvas = std::make_shared<Canvas>(glm::vec2(0.0f, 0.0f), glm::vec2(getWidth(), getHeight()));

    auto img1 = std::make_shared<Image>("background.pvr");
    auto img2 = std::make_shared<Image>("test.pvr");
    canvas->attach(img1);
    img1->attach(img2);
    canvas->attach(img2);
    return pvr::Result::Success;
}

pvr::Result::Enum IQOptionTest::releaseView()
{
    canvas.reset();
    screen.reset();
    return pvr::Result::Success;
}

pvr::Result::Enum IQOptionTest::renderFrame()
{
    pvr::uint64 currentTime = this->getTime() - this->getTimeAtInitApplication();
    float deltaTime = (currentTime - lastTime) * 0.001f;
    lastTime = currentTime;
    //TODO: animate anything here
    canvas->render(screen);
    return pvr::Result::Success;
}

void IQOptionTest::eventMappedInput(pvr::SimplifiedInput::Enum key)
{
    //TODO:
    //canvas->injectMappedInput(key);
}

std::auto_ptr<pvr::Shell> pvr::newDemo()
{
    return std::auto_ptr<pvr::Shell>(new IQOptionTest());
}
