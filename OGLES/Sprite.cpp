#include "Sprite.h"
#include "RenderGroup.h"
#include <algorithm>

void Sprite::setPosition(glm::vec2 xy)
{
    SpriteGroup::setPosition(xy);
    for (auto instance : instances)
        instance->markTransformDirty();
}

void Sprite::setScale(glm::vec2 wh)
{
    scale = wh;
    //TODO: recalculate size. size = originalSize*scale;
    for (auto instance : instances)
        instance->markTransformDirty();
}

void Sprite::setRotation(float angleDegrees)
{
    rotation = angleDegrees;
    for (auto instance : instances)
        instance->markTransformDirty();
}

void Sprite::setVisible(bool flag)
{
    if (visible != flag) {
        visible = flag;
        for (auto instance : instances)
            instance->markVisibleDirty();
    }
}

void Sprite::setZIndex(int index)
{
    if (zIndex != index) {
        zIndex = index;
        for (auto instance : instances)
            instance->markZDirty();
    }
}