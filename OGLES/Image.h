#ifndef IMAGE_H
#define IMAGE_H

#include "Sprite.h"

class Image : public Sprite {
    public:
        Image(const char* filename);
        virtual ~Image(void);
    protected:
        virtual RenderGroup* addSubtree(RenderGroup* parent);
};

#endif /* end of include guard: IMAGE_H */
