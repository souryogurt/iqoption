#ifndef RENDERINSTANCE_H
#define RENDERINSTANCE_H
#include "../External/glm/glm.hpp"

class RenderGroup;
class RenderInstance {
    public:
        virtual void validateTransform(void) = 0;
        virtual void validateVisible(void) = 0;
        virtual void validateZ(void) = 0;
        virtual void sortAndAssignZ(int level, int& order) = 0;
        virtual ~RenderInstance(void) {};
        glm::mat4 transform;
        bool visible;
        int zIndex;
        RenderGroup* parent;
    protected:
        virtual RenderGroup* getRoot(void) = 0;
        RenderInstance(RenderGroup* p) : parent(p), visible(true), zIndex(0) {};
};

#endif /* end of include guard: RENDERINSTANCE_H */
