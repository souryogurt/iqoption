#ifndef IQCANVAS_H
#define IQCANVAS_H
#include <memory>
#include "Sprite.h"
#include "PVRShell/PVRShell.h"
#include "PVRApi/PVRApi.h"

class IQCanvas : public Sprite {
    public:
        IQCanvas(pvr::Shell& application);
        void renderFrame(const pvr::api::Fbo&);
    protected:
        pvr::Shell& shell;
        pvr::api::SecondaryCommandBuffer updateBuffer;
        pvr::api::CommandBuffer drawBuffer;
        pvr::api::AssetStore assetStore;

        pvr::api::Buffer imageVbo;
        pvr::api::EffectApi fontEffect;
        void CreateImageVBO(void);
        void CreatePipelineAndRenderPass(void);
};

#endif /* end of include guard: IQCANVAS_H */
