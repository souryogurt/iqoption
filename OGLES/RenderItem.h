#ifndef RENDERITEM_H
#define RENDERITEM_H

class RenderGroup;
class RenderInstance {
    protected:
        RenderGroup* parent;
        virtual RenderGroup* getRoot(void) = 0;
        glm::mat4 transform;
        virtual void validateTransform(void) = 0;
        bool visible;
        RenderInstance(RenderGroup* p): parent(p) {};
        virtual ~RenderInstance(void) {};
};

class Shape: public RenderInstance {
    protected:
        Shape(RenderGroup* p): RenderInstance(p) {};
        virtual validateTransform(void) {
            transform = parent->transform;
        };
        virtual RenderGroup* getRoot(void) {
            auto root = parent;
            while (root->parent != nullptr)
                root = root->parent;
            return root;
        };
        virtual ~Shape(void) {
            getRoot()->removeShape(this);
        };
};

class SpriteGroup;
class RenderGroup: public RenderInstance {
    public:
        RenderGroup* addRenderGroup(SpriteGroup* sprite) {
            auto instance = new RenderGroup(sprite, this);
            //Insert sorted based on zIndex? Or sort later?
            //look std::upper_bound()
            items.push_front(instance);
            return instance;
        };
        void addShape(void) {
            auto instance = new Shape(this);
            //Insert sorted based on zIndex? Or sort later?
            //look std::upper_bound()
            items.push_front(instance);
            gotRoot()->addShape(instance);
        };
        void removeSubtree(RenderInstance* tree) {
            items.remove(tree);
        };
        void markTransformDirty(void) {
            if (!isTransformDirty) {
                isTransformDirty = true;
                getRoot()->addDirtyTransform(this);
            }
        };
        virtual validateTransform(void) {
            transform = glm::translate(parent->transform, owner->getPosition().x, owner->getPosition().y, 0.0f);
            transform = glm::rotate(transform, owner->getRotation(), glm::vec3(0.0f, 0.0f, 1.0f));
            transform = glm::scale(transform, owner->getScale().x, owner->getScale().y, 1.0f);
            for (auto item : items)
                item->validateTransform();
        };
        ~RenderGroup(void) {
            getRoot()->removeTransform(this);
        };
    protected:
        virtual RenderGroup* getRoot(void) {
            auto root = this;
            while (root->parent != nullptr)
                root = root->parent;
            return root;
        };
        virtual void addShape(Shape*) {
            //TODO: abort. algorightm is wrong
        };
        virtual void removeShape(Shape*) {
            //TODO: abort. algorightm is wrong
        };
        virtual void addDirtyTransform(RenderGroup*) {
            //TODO: abort. algorightm is wrong
        };
        virtual void removeTransform(RenderGroup*) {
            //TODO: abort. algorightm is wrong
        };
        SpriteGroup* owner;
        bool isTransformDirty;
        bool isVisibleDirty;
        RenderGroup(SpriteGroup* spriteGroup, RenderGroup* p): RenderInstance(p), owner(spriteGroup), isTransformDirty(false) {
            markTransformDirty();
        };
        std::forward_list < std::unique_ptr<RenderInstance> items;
};


class ViewBox: public RenderGroup {
    protected:
        ViewBox(SpriteGroup* spriteGroup, RenderGroup* p): RenderGroup(spriteGroup, p) {};
        virtual validateTransform(void) {
            transform = glm::ortho(owner->getPosition().x, owner->getSize().x, owner->getPosition().y, owner->getSize().y);
            for (auto item : items)
                item->validateTransform();
        };
};

class Canvas;
class Painter: public ViewBox {
    public:
        Painter(Canvas* canvas): ViewBox(canvas, nullptr) {};
    protected:
        std::forward_list<RenderGroup*> dirtyTransforms;
        std::list<Shape*> displayList;
        virtual void addShape(Shape* shape) {
            displayList.push_front(shape);
        };
        virtual void removeShape(Shape* shape) {
            displayList.remove(shape);
        };
        virtual void addDirtyTransform(RenderGroup* group) {
            dirtyTransforms.push_front(group);
        };
        virtual void removeTransform(RenderGroup* group) {
            dirtyTransforms.remove(group);
        };
        void render(void) {
            //recalculate transforms if anything moved/scaled/rotated
            for (auto group : dirtyTransforms) {
                if (group->isTransformDirty) {
                    auto topmostDirty = group;
                    auto current = group;
                    while (current->parent) {
                        current = current->parent;
                        if (current->isTransformDirty)
                            topmostDirty = current;
                    }
                    topmostDirty->validateTransform();
                }
            }
            dirtyTransforms.clear();
        };
};

#endif /* end of include guard: RENDERITEM_H */
