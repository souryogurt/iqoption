#include "IQCanvas.h"
IQCanvas::IQCanvas(pvr::Shell& application) : shell(application), isDirty(true)
{
    //IQCanvas is only class that has one instance by default
    instances.push_front(instance(nullptr));
    instances.front()->mvpMatrix = glm::ortho(0, shell.getWidth(), 0, shell.getHeight());
    assetStore.init(shell);
    drawBuffer = shell.getGraphicsContext()->createCommandBufferOnDefaultPool();
    updateBuffer = shell.getGraphicsContext()->createSecondaryCommandBufferOnDefaultPool();
}

void IQCanvas::CreateImageVBO(void)
{
    const pvr::float32 verts[] = {
        /*  Position       texCoord*/
        -1.f, 1.f, 0.f, 1.f, 0.0f, 1.0f,
        -1.f, -1.f, 0.f, 1.f, 0.0f, 0.0f,
        1.f, 1.f, 0.f, 1.f, 1.0f, 1.0f,
        -1.f, -1.f, 0.f, 1.f, 0.0f, 0.0f,
        1.f, -1.f, 0.f, 1.f, 1.0f, 0.0f,
        1.f, 1.f, 0.f, 1.f, 1.0f, 1.0f,
    };
    imageVbo = shell.getGraphicsContext()->createBuffer(sizeof(verts), pvr::types::BufferBindingUse::VertexBuffer,
               pvr::types::BufferUse::DEFAULT);
    if (imageVbo.isNull())
        throw std::runtime_error("Can't create image VBO");
    imageVbo->update((void*)verts, 0, sizeof(verts));
}

void IQCanvas::CreatePipelineAndRenderPass(void)
{
    //One PFX file might contain zero or many effects
    //Canvas has one and only one pfx file that describes all effects for all types of sprites
    pvr::assets::PfxReader effectParser;
    std::string parseError;
    if (!effectParser.parseFromFile(shell.getAssetStream("FontEffect.pfx", false), parseError))
        throw std::runtime_error(parseError);

    //This is really not required.
    //Text sprite will do this by self. It will request the effect with
    //a name of font e.g. Arial, Ubuntu etc.
    //Such font effects will use the same shaders
    //But will have different font textures specified
    pvr::assets::Effect fontEffectDescription;
    if (!effectParser.getEffect(fontEffectDescription, "FontEffect"))
        throw std::runtime_error("Can't find FontEffect effect in pfx");

    /*


    //This not cool code is required because EffectApi_ has a bug. it calls setBinding with arraySize = 0
    //In real life I would fix it. But can't do this right now because IQ Option software guys
    //that will review this code will use not patched version of SDK
    pvr::api::DescriptorSetLayoutCreateParam descSetLayoutInfo;
    for (size_t i = 0; i < fontEffectDescription.textures.size();i++){
        descSetLayoutInfo.setBinding(i, pvr::types::DescriptorType::CombinedImageSampler, 1, pvr::types::ShaderStageFlags::Fragment);
    }
    pvr::api::GraphicsPipelineCreateParam pipelineDesc;
    m_descriptorSetLayout = m_context->createDescriptorSetLayout(descSetLayoutInfo);
    PipelineLayoutCreateParam pipeLayoutCreateInfo;
    pipeLayoutCreateInfo.addDescSetLayout(m_descriptorSetLayout);
    pipeDesc.pipelineLayout = m_context->createPipelineLayout(pipeLayoutCreateInfo);



    //This is depends on mesh we are using
    //[X,Y,Z,-,U,V] - 6 * f32
    pipelineDesc.vertexInput.setInputBinding(0, sizeof(pvr::float32) * 6);
    pipelineDesc.vertexInput.addVertexAttribute(0, pvr::api::VertexAttributeInfo(0, pvr::types::DataType::Float32, 3, 0));
    pipelineDesc.vertexInput.addVertexAttribute(0, pvr::api::VertexAttributeInfo(1, pvr::types::DataType::Float32, 2,
            sizeof(pvr::float32) * 4));
    pipelineDesc.inputAssembler.setPrimitiveTopology(pvr::types::PrimitiveTopology::TriangleList);

    //Using alpha for blending
    pvr::api::pipelineCreation::ColorBlendAttachmentState attachmentState(true, pvr::types::BlendFactor::SrcAlpha,
            pvr::types::BlendFactor::OneMinusSrcAlpha, pvr::types::BlendOp::Add, pvr::types::ColorChannel::All);
    pipelineDesc.colorBlend.setAttachmentState(0, attachmentState);
    pipelineDesc.depthStencil.setDepthTestEnable(false);
    pipelineDesc.depthStencil.setDepthWrite(false);
    pipelineDesc.rasterizer.setCullFace(pvr::types::Face::None);
    //pipelineDesc.renderPass = ????
    //pipelineDesc.subPass = ???




    fontEffect = shell.getGraphicsContext()->createEffectApi(fontEffectDescription, pipelineDesc, assetStore);
    if (!fontEffect.isValid())
        throw std::runtime_error("Can't compile effect");
    */
}

bool IQCanvas::update(float dtSeconds)
{
    //Validate transformation matrix
    if (!dirtyList.empty()) {
        updateBuffer->beginRecording(fbo, 0);
        for (auto shape : dirtyList)
            if (shape->mvpDirty)
                shape->validate();
        dirtyList.clear();
        updateBuffer->endRecording();
        //TODO:
        //cmds->enqueueSecondaryCmds(updateBuffer);
        //updateBuffer->clear();
        //OR:
        //if(changed){
        //  cmds->beginRenderPass(fbo,0)
        //  cmds->updateBuffer()
        //  cmds->
    }
    // then set isDirty flag if we need to reconstruct the
    // command buffer
    return isDirty;
}

void IQCanvas::renderFrame(const pvr::api::Fbo& framebuffer)
{
    updateBuffer->beginRecording(framebuffer, 0);
    for (auto shape : dirtyList) {
        if (shape->mvpDirty) {
            auto topDirty = shape;
            while (shape->parent != nullptr) {
                shape = shape->parent;
                if (shape->mvpDirty)
                    topDirty = shape;
            }
            topDirty()->validate();
        }
    }
    dirtyList.clear();
    updateBuffer->endRecording();

    //if (dirtyRect!=empty) {
    //  drawBuffer->beginRecording()
    //  drawBuffer->enqueueSecondaryCmds(updateBuffer);
    //  drawBuffer->beginRenderPass(fbo, dirtyRect, true, clearColor)
    //  foreach shape in shapeList
    //      if(shape->boundingBox intersects dirtyRect)
    //          draw shape and all childs
    //  endRenderPass();
    //  updated = true;
    //}
    //
    //or
    //
    //if(shapelist is dirty){ //shapes was added or removed or visible changed
    // drawBuffer->beginRecording();
    // drawBuffer->enqueueSecondaryCmds(updateBuffer);
    // drawBuffer->beginRenderPass(fbo, canvasRect, true, clearColor);
    // foreach shape in shapelist{
    //  if(shape->isVisible){
    //      if(shape->pipeline != drawBuffer->getPipeline())
    //          drawBuffer->bindPipeline(shape->pipeline);
    //      if(shape->texture != drawBuffer->getBoundTexture())
    //          drawBuffer->bindTexture(shape->texture);
    //  }
    // }
    // drawBuffer->endRenderPass();
    // drawBuffer->endRecording();
    // updated = true;
    //}
    //
    drawBuffer->beginRecording();
    drawBuffer->enqueueSecondaryCmds(updateBuffer);
    pvr::Rectanglei screenRect(0, 0, getWidth(), getHeight());
    glm::vec4 clearColor(0.3f, 0.7f, 1.0f, 1.0f);
    cmdBuffer->beginRenderPass(screen, screenRect, true, clearColor);
    //for each shape in displayed list
    //if(shape->pipeline != currentPipeline) shape->bindPipeline()
    //if(shape->parameters != currentParameters) {
    //  setUniform(
    //}
    cmdBuffer->endRenderPass();
    drawBuffer->endRecording();

    drawBuffer->submit();
}

void IQCanvas::render(pvr::api::CommandBuffer& cmds)
{
    //TODO: actually reconstruct the command buffer
    //and enque it to cmds
    cmds->
    cmds->enqueueSecondaryCmds(drawBuffer);
    isDirty = false;
}
