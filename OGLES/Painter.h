#ifndef PAINTER_H
#define PAINTER_H
#include "RenderGroup.h"
#include <forward_list>
#include <list>
#include "PVRApi/PVRApi.h"

class Painter: public RenderGroup {
    public:
        Painter(SpriteGroup* canvas);
        void render(pvr::api::Fbo& fbo);
    protected:
        bool shapesChanged;
        std::forward_list<RenderGroup*> dirtyTransforms;
        std::forward_list<RenderGroup*> dirtyVisible;
        std::forward_list<RenderGroup*> dirtyZIndex;
        std::list<Shape*> displayList;
        virtual void addShape(Shape* shape);
        virtual void removeShape(Shape* shape);
        virtual void addDirtyTransform(RenderGroup* group);
        virtual void addDirtyVisible(RenderGroup* group);
        virtual void addDirtyZIndex(RenderGroup* group);
        virtual void removeRenderGroup(RenderGroup* group);
        void validateDirtyTransforms(void);
        bool validateDirtyVisible(void);
        bool validateDirtyZIndex(void);

};

#endif /* end of include guard: PAINTER_H */
