#include "Canvas.h"
#include "Sprite.h"
#include "ViewBox.h"

Canvas::Canvas(glm::vec2 origin, glm::vec2 wh):
    SpriteGroup(), renderer(this)
{
    position = origin;
    size = wh;
    instances.push_front(renderer.addViewBox(this));
}

void Canvas::setPosition(glm::vec2 xy)
{
    SpriteGroup::setPosition(xy);
    renderer.markTransformDirty();
}

void Canvas::setSize(glm::vec2 wh)
{
    size = wh;
    renderer.markTransformDirty();
}

void Canvas::render(pvr::api::Fbo& fbo)
{
    renderer.render(fbo);
}