#ifndef RENDERGROUP_H
#define RENDERGROUP_H
#include "RenderInstance.h"
#include <memory>
#include <list>

class SpriteGroup;
class Shape;
class ViewBox;
class RenderGroup: public RenderInstance {
        friend class Painter;
    public:
        RenderGroup* addRenderGroup(SpriteGroup* sprite);
        ViewBox* addViewBox(SpriteGroup* sprite);
        void addShape(void);
        void removeSubtree(RenderInstance* tree);
        void markTransformDirty(void);
        void markVisibleDirty(void);
        void markZDirty(void);
        virtual ~RenderGroup(void);
        virtual void validateTransform(void);
        virtual void validateVisible(void);
        virtual void validateZ(void);
        virtual void sortAndAssignZ(int level, int& order);
        virtual void removeShape(Shape*);
    protected:
        virtual RenderGroup* getRoot(void);
        virtual void addShape(Shape*);
        virtual void addDirtyTransform(RenderGroup*);
        virtual void addDirtyVisible(RenderGroup*);
        virtual void addDirtyZIndex(RenderGroup*);
        virtual void removeRenderGroup(RenderGroup*);
        SpriteGroup* owner;
        bool isTransformDirty;
        bool isVisibleDirty;
        bool isZDirty;
        RenderGroup(SpriteGroup* spriteGroup, RenderGroup* p);
        std::list <std::unique_ptr<RenderInstance>> items;
};

#endif /* end of include guard: RENDERGROUP_H */
